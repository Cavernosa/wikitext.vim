# mediawiki.vim
![](demo.png)

Wikitext/Mediawiki syntax highlighting for Vim. Most of the wikitext colors are based on the [CodeMirror styling](https://meta.wikimedia.org/wiki/Community_Tech/Wikitext_editor_syntax_highlighting#Color_and_style_customization) which is used by default in the Mediawiki code editor.

## Progress
Usable for basic editing. Features will be added as I need them (or if you send a patch :))

### Done
- Internal/External Links
- Bold, italic, and bold italic limited support (only plain text)
- Bulleted lists
- Templates (inline and block)
- Magic behavior switches (e.g. `__NOTOC__`)
- Most magic variables (e.g. `{{PAGENAME}}`)
- HTML
    - Extension Tags: gallery, ref, references, etc.
    - Entities, Comments

### To do
- Identing (`:`)
- Signature (`~~~~`)
- Headings (`=`)
- Magic parser functions (e.g. `{{#ifeq:}}`)
- Variables (`{{{}}}`)
- Pre-formatted text (`nowiki` & `pre`)
- Tables (`{|`, `|-`, `|}`, ...)

## Installing
Copy `mediawiki.vim` into `~/.vim/syntax/`

Alternatively, you can create a package in `~/.vim/pack/`. See `:help packages` for details.

## Configuration
### Ref Highlighting
Ref Highlighting is an experimental feature which turns most normal text inside `<ref>`s green so its easier to find what's content and what's a reference. 
If you don't like that and want the normal colors, add `let wikitext_no_ref_hl=1` in Vim config to disable it.
